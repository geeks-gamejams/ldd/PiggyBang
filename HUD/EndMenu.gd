extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func set_end_title(winner):
	print(winner)
	if winner == "Piggy":
		$free.show()
	elif winner == "Trader":
		$dead.show()
	
	$ColorRect.show()
	$VBoxContainer.show()

	$ColorRect.show()
	get_tree().paused = true


func _on_resetb_pressed():
	get_tree().paused = false
	get_tree().reload_current_scene()


func _on_quit_pressed():
	get_tree().quit() # default behavior
