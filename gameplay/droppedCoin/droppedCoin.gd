extends Node2D

var velocity
var acceleration = 200
var timeSinceStart = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	$Timer.start(2)
	$Timer.connect("timeout", self, "queue_free")
	var angle = rand_range(-PI,0)
	velocity = 200*Vector2(cos(angle), sin(angle))

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	velocity.y += acceleration*delta
	position += velocity*delta
	timeSinceStart += delta
	if timeSinceStart < 2:
		$Sprite.modulate = Color(1, 1, 1, 1-0.5*timeSinceStart)
	else:
		$Sprite.modulate = Color(1, 1, 1, 0)
