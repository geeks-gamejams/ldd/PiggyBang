extends Node2D

export var buttonCooldown = 10

var texture_up = preload("res://gameplay/hammer/hammer_v02/hammer_bouton.png")
var texture_down = preload("res://gameplay/hammer/hammer_v02/hammer_bouton_pressed.png")

# Called when the node enters the scene tree for the first time.
func _ready():
	$Timer.connect("timeout", self, "on_timeout")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func on_button_activated(body):
	if body.is_in_group("piggy"):
		$Timer.start(buttonCooldown)
		$CollisionShape2D.set_deferred("disabled", true)
		$hammer3.texture = texture_down

func on_timeout():
	$CollisionShape2D.set_deferred("disabled", false)
	$hammer3.texture = texture_up