extends Node2D

#COOLDOWN
var timer = null
export var fireColdown = .4
var canShot = true

export var startingRailPosition = 100
export var endingRailPosition = 1800
export var speed = 650
export var coinSpeed = 900

var coin = preload("res://gameplay/coin/coin.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	timer = Timer.new()
	timer.set_one_shot(true)
	timer.set_wait_time(fireColdown)
	timer.connect("timeout",self,"_fire_ready")
	add_child(timer)

func _fire_ready():
	canShot = true
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):	
	var isWalkingLeft = Input.is_action_pressed("traderLeft")
	var isWalkingRight = Input.is_action_pressed("traderRight")
	move_local_x(delta*speed*(-int(isWalkingLeft)+int(isWalkingRight)))
	if position.x>endingRailPosition:
		position.x = endingRailPosition
	if position.x<startingRailPosition:
		position.x = startingRailPosition
	if canShot and (Input.is_action_just_pressed("shoot")):
		var anim = "fire"
		$trader_02_anim01.frame = 0
		$trader_02_anim01.play(anim)
		
		var coinInstance = coin.instance()
		coinInstance.position = position
		var directionVector = get_viewport().get_mouse_position()-position
		if directionVector.y < 0:
			directionVector.y = 0
		coinInstance.linear_velocity = directionVector.normalized()*coinSpeed
		
		get_parent().add_child(coinInstance)
		
		canShot = false
		timer.start()
		
#		coinInstance.connect("body_entered", piggy, "on_collision")
