 extends KinematicBody2D

export var maxCoins = 20
export var coinsCount = 10
export var coinSpeed = 10
# This demo shows how to build a kinematic controller.

# Member variables
const GRAVITY = 1800.0 # pixels/second/second

# Angle in degrees towards either side that the player can consider "floor"
const FLOOR_ANGLE_TOLERANCE = 40
const WALK_FORCE = 2300
const WALK_MIN_SPEED = 10
const WALK_MAX_SPEED = 500
const STOP_FORCE = 3300
const JUMP_SPEED = 1000
const JUMP_MAX_AIRBORNE_TIME = 0.2

const SLIDE_STOP_VELOCITY =0.0 # one pixel/second
const SLIDE_STOP_MIN_TRAVEL = 0.0 # one pixel

var velocity = Vector2()
var on_air_time = 100
var jumping = false

var prev_jump_pressed = false
var is_right = false
var coin = preload("res://gameplay/coin/coin.tscn")
var droppedCoin = preload("res://gameplay/droppedCoin/droppedCoin.tscn")
var jumpSound = load("res://sounds/CochonJump.wav")
var deathSound = load("res://sounds/CochonMort.wav")
var injureSound = load("res://sounds/CochonSouffre.wav")
var idleSound = load("res://sounds/CochonNormal.wav")
var walkSound = load("res://sounds/CochonNormal.wav")
var rireSound = load("res://sounds/CochonContent.wav")
var injured = false
var gui = null

signal onLifeChanged(coinsCount)
signal endGame(winner)

#AUDIO
#var audio_player = $AudioStreamPlayer2D 


func _ready():
	emit_signal("onLifeChanged",float(coinsCount)/maxCoins)

func _die():
	_pplay(deathSound)
	emit_signal("endGame","Trader")
	
	
func drop_gold():
	if coinsCount > 1:
		_pplay(rireSound)
		coinsCount -= 1
		_refresh_gui()
		var coinInstance = droppedCoin.instance()
		coinInstance.position = position
		get_parent().add_child(coinInstance)
		injured = true
		$Sprite.play("hammer")
	else:
		emit_signal("endGame","Piggy")
		
		
	
func on_collision():
	injured = true
	$Sprite.play("coin")
	_pplay(injureSound)
	coinsCount += 1
	
	_refresh_gui()
	
	if coinsCount > maxCoins-1:
		_die()

func _refresh_gui():
	emit_signal("onLifeChanged",float(coinsCount)/maxCoins)
	
func _physics_process(delta):
	
	# Create forces
	var force = Vector2(0, GRAVITY)
	
	var walk_left = Input.is_action_pressed("move_left")
	var walk_right = Input.is_action_pressed("move_right")
	var jump = Input.is_action_pressed("jump")
	
	var stop = true
	
	if walk_left and walk_right:
		
		pass
	elif walk_left:
		if is_right:
			is_right = false
			$Sprite.scale.x = -$Sprite.scale.x
		if velocity.x <= WALK_MIN_SPEED and velocity.x > -WALK_MAX_SPEED:
			force.x -= WALK_FORCE
			stop = false
	elif walk_right:
		if not is_right:
			is_right = true
			$Sprite.scale.x = -$Sprite.scale.x
		if velocity.x >= -WALK_MIN_SPEED and velocity.x < WALK_MAX_SPEED:
			force.x += WALK_FORCE
			stop = false
	
	if stop:
		
		var vsign = sign(velocity.x)
		var vlen = abs(velocity.x)
		
		vlen -= STOP_FORCE * delta
		if vlen < 0:
			vlen = 0
		
		velocity.x = vlen * vsign
		# Integrate forces to velocity
	velocity += force *delta
	# Integrate velocity into motion and move
	velocity = move_and_slide(velocity, Vector2(0, -1))
#	if get_slide_count() != 0:
#		for i in range (0, get_slide_count()):
#			if get_slide_collision(i).collider.is_in_group("coin"):
#				print("swann fait nimportequoi")
	

	for idx  in range(0,get_slide_count()):
		pass
		
	if is_on_floor():
		on_air_time = 0
		
	if jumping and velocity.y > 0:
		# If falling, no longer jumping
		jumping = false
	
	if on_air_time < JUMP_MAX_AIRBORNE_TIME and jump and not prev_jump_pressed and not jumping:
		# Jump must also be allowed to happen if the character left the floor a little bit ago.
		# Makes controls more snappy.
		_pplay(jumpSound)
		
		velocity.y = -JUMP_SPEED
		jumping = true
	
	on_air_time += delta
	prev_jump_pressed = jump
	
	var new_anim = ""
	
	if not is_on_floor() :
		new_anim = "jump"
	elif abs(velocity.x) < 0.1:
		if $AudioStreamPlayer2D.stream == walkSound:
			$AudioStreamPlayer2D.stop()
		new_anim = "idle"
	elif abs(velocity.x) > 0.1 and abs(velocity.y) <0.1:
		new_anim = "run"
		_play(walkSound)
		
	if $Sprite.animation != new_anim and not injured:
		$Sprite.play(new_anim)
func _pplay(sound):
	$AudioStreamPlayer2D.stream = sound
	$AudioStreamPlayer2D.play()
	
func _play(sound):
	if  ($AudioStreamPlayer2D.stream == sound and $AudioStreamPlayer2D.playing) or injured:
		pass
	else:
		$AudioStreamPlayer2D.stream = sound
		$AudioStreamPlayer2D.play()
		
	
	



func _on_Sprite_animation_finished():
	injured = false
